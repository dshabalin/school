<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <title>Home</title>
</head>
<body>
<center>
    <h1>${headerMessage}</h1>

    <form:form method="POST" action="/editStudent" enctype="multipart/form-data">

        <table>
            <input type="hidden" value="${student.id}" name="id"/>
            <tr>
                <td><label path="Name">Имя</label></td>
                <td><input type="text" name="name" value="${student.name}"/></td>
            </tr>
            <tr>
                <td><label path="Surname">Фамилия</label></td>
                <td><input name="surname" value="${student.surname}"/></td>
            </tr>
            <tr>
                <td><label path="Avatar">Фотография:</label></td>
                <td>
                    <img src="${pageContext.request.contextPath}/avatar?avatar=${student.avatar}"
                         style="max-height: 200px; max-width: 200px;"/>
                </td>
                <td>
                    <input type="file" name="avatar"/>
                </td>
            </tr>
            <tr>
                <td><input class="btn btn-primary" type="submit" value="Сохранить"></td>
            </tr>
        </table>
    </form:form>
</center>
</body>
</html>